package com.example.mateu.tictactoe;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Play extends AppCompatActivity {
    private Button play;
    private Button logout;
    private Button pvp;
    private Button pve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        play = (Button) findViewById(R.id.play);
        logout = (Button) findViewById(R.id.logout);
        pvp = (Button) findViewById(R.id.PVP);
        pve = (Button) findViewById(R.id.PVE);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Play.this, GameActivity.class);
                Play.this.startActivity(myIntent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvp.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorButtonClick));
                pve.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorButton));
                pvp.setTag("true");
                pve.setTag("false");
            }
        });

        pve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pve.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorButtonClick));
                pvp.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorButton));
                pve.setTag("true");
                pvp.setTag("false");
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
