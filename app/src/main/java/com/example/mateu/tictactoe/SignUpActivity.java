package com.example.mateu.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {
    private EditText login;
    private EditText email;
    private EditText password;
    private Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        login = (EditText) findViewById(R.id.loginSignUp);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.passwordSignUp);
        signUp = (Button) findViewById(R.id.signUpButton);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check(login) && check(password)) {
                    if(checkEmail(email)) {
                        finish();
                    }
                    else
                        Toast.makeText(SignUpActivity.this,"Enter valid email address!",Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(SignUpActivity.this,"Fill all fields to continue!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean check(EditText txt) {
        return txt.getText().length() > 0;
    }


    public boolean checkEmail(EditText txt) {
        if (txt.getText().toString().contains("@")){
            String tab[];
            tab = txt.getText().toString().split("@");
            if(tab.length == 2 && tab[1].contains(".") && tab[0].length()>0 && tab[1].length()>1){
                return true;
            }
        }
        return false;
    }


}
