package com.example.mateu.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private Button logInButton;
    private TextView signUpButton;
    private EditText name;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        logInButton = (Button) findViewById(R.id.LogInButton);
        signUpButton = (TextView) findViewById(R.id.signUp);
        name = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check(name) && check(password)) {
                    Intent myIntent = new Intent(LoginActivity.this, Play.class);
                    LoginActivity.this.startActivity(myIntent);
                }
                else
                    Toast.makeText(LoginActivity.this,"Fill all fields to continue!",Toast.LENGTH_SHORT).show();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                LoginActivity.this.startActivity(myIntent);
            }
        });

    }

    public boolean check(EditText txt) {
        return txt.getText().length() != 0;
    }


}
